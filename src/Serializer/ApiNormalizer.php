<?php

namespace App\Serializer;

use App\Entity\BasicRate;
use App\Entity\Booking;
use App\Entity\Customer;
use App\Entity\CustomerContact;
use App\Entity\CustomerIdentification;
use App\Entity\Document;
use App\Entity\Menu;
use App\Entity\Proforma;
use App\Entity\Transaction;
use App\Entity\User;
use App\Entity\TerminalGetin;
use App\Entity\TerminalGetOut;
use App\Entity\CustomerFreeField;
use App\Service\PeriodCalculator;
use App\Service\TimeRangeLogicService;
use App\Repository\ContainerRepository;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use ApiPlatform\Serializer\ItemNormalizer;
use App\Entity\Coupling;
use App\Entity\Staff;
use App\Entity\TransportOrder;
use NumberFormatter;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Vich\UploaderBundle\Storage\StorageInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\DependencyInjection\Attribute\AsDecorator;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;


//#[AsDecorator(decorates: ItemNormalizer::class)]
final class ApiNormalizer implements NormalizerInterface, DenormalizerInterface, SerializerAwareInterface
{
    private $decorated;
    use NormalizerAwareTrait;

    public function __construct(
        NormalizerInterface $decorated,
        private RequestStack $requestStack,
        private EntityManagerInterface  $em,
        private EntityManagerInterface $entityManagerInterface
    )
    {
        if (!$decorated instanceof DenormalizerInterface) {
            throw new \InvalidArgumentException(sprintf('The decorated normalizer must implement the %s.', DenormalizerInterface::class));
        }
        $this->decorated = $decorated;
    }

    public function normalize(mixed $object, string $format = null, array $context = [])
    {

        $data = $this->decorated->normalize($object, $format, $context);
        if (is_array($data) && $object instanceof Document) {
            $data['compteAffaire'] = $data['compteAffaire'] ?? "";
            $data['compteEvent'] = $data['compteEvent'] ?? "";
            $data['compteDerEv'] = $data['compteDerEv'] ?? "";
            $data['numFiche'] = $data['numFiche'] ?? "";
            $data['libelleCiv'] = $data['libelleCiv'] ?? "";
            $data['proprioVehicule'] = $data['proprioVehicule'] ?? "";
            $data['nom'] = $data['nom'] ?? "";
            $data['prenom'] = $data['compteAffaire'] ?? "";
            $data['numVoie'] = $data['numVoie'] ?? "";
            $data['compAdresse'] = $data['compAdresse'] ?? "";
            $data['codePostal'] = $data['codePostal'] ?? "";
            $data['ville'] = $data['ville'] ?? "";
            $data['telDom'] = $data['telDom'] ?? "";
            $data['telPor'] = $data['telPor'] ?? "";
            $data['email'] = $data['email'] ?? "";
            $data['dateCirculation'] = $data['dateCirculation'] ?? "";
            $data['dateAchat'] = $data['dateAchat'] ?? "";
            $data['dateDerEnv'] = $data['dateDerEnv'] ?? "";
            $data['libelleMarque'] = $data['libelleMarque'] ?? "";
            $data['libelleModele'] = $data['libelleModele'] ?? "";
            $data['version'] = $data['version'] ?? "";
            $data['vin'] = $data['vin'] ?? "";
            $data['immatriculation'] = $data['immatriculation'] ?? "";
            $data['typeProspect'] = $data['typeProspect'] ?? "";
            $data['kilometrage'] = $data['kilometrage'] ?? "";
            $data['energie'] = $data['energie'] ?? "";
            $data['vendeurVn'] = $data['vendeurVn'] ?? "";
            $data['vendeurVO'] = $data['vendeurVO'] ?? "";
            $data['commentaireFacture'] = $data['commentaireFacture'] ?? "";
            $data['typeVnVo'] = $data['typeVnVo'] ?? "";
            $data['numDVnVo'] = $data['numDVnVo'] ?? "";
            $data['interVen'] = $data['interVen'] ?? "";
            $data['DateEv'] = $data['DateEv'] ?? "";
            $data['originEnv'] = $data['originEnv'] ?? "";
            $data['action'] = "";
        }
        return $data;
    }

    public function supportsNormalization(mixed $data, string $format = null): bool
    {
        return $this->decorated->supportsNormalization($data, $format);
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $this->decorated->supportsDenormalization($data, $type, $format);
    }

    public function denormalize($data, string $type, string $format = null, array $context = [])
    {

        $instance = new $type();
        return $this->decorated->denormalize($data, $type, $format, $context);
    }

    public function setSerializer(SerializerInterface $serializer)
    {
        if($this->decorated instanceof SerializerAwareInterface) {
            $this->decorated->setSerializer($serializer);
        }
    }
}