<?php

namespace App\Form;

use App\Entity\Document;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DocumentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('compteAffaire')
            ->add('compteEvent')
            ->add('compteDerEv')
            ->add('numFiche')
            ->add('libelleCiv')
            ->add('proprioVehicule')
            ->add('nom')
            ->add('prenom')
            ->add('numVoie')
            ->add('compAdresse')
            ->add('codePostal')
            ->add('ville')
            ->add('telDom')
            ->add('telPor')
            ->add('telJob')
            ->add('email')
            ->add('dateCirculation', null, [
                'widget' => 'single_text',
            ])
            ->add('dateAchat', null, [
                'widget' => 'single_text',
            ])
            ->add('dateDerEnv', null, [
                'widget' => 'single_text',
            ])
            ->add('libelleMarque')
            ->add('libelleModele')
            ->add('version')
            ->add('vin')
            ->add('immatriculation')
            ->add('typeProspect')
            ->add('kilometrage')
            ->add('energie')
            ->add('vendeurVn')
            ->add('vendeurVO')
            ->add('commentaireFacture')
            ->add('typeVnVo')
            ->add('numDVnVo')
            ->add('interVen')
            ->add('DateEv', null, [
                'widget' => 'single_text',
            ])
            ->add('originEnv')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Document::class,
        ]);
    }
}
