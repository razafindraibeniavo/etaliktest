<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class PaginationService
 * @package App\Service
 */
class PaginationService
{
    public function __construct(
        private RequestStack $requestStack,
    )
    {
    }

    /**
     * @param $data
     * @return array
     */
    public function getMetadataPagination($data){
        $count = count($data);
        $nbPageReturned = 0;
        if($count > 0){
            $nbPageReturned = $data->getTotalItems() / /*count($data)*/$data->getItemsPerPage();
        }
        $nbPageReturned = ceil($nbPageReturned);
        $links = [];
        for($i=1 ; $i<= $nbPageReturned;$i++){
            $links[] = [
                "url" => "?/page=$i",
                "active" => $i == $data->getCurrentPage(),
                "page" => $i
            ];
        }
        return [
            "links" => $links,
            'first' => 1,
            'current' => $data->getCurrentPage(),
            'last' => $data->getLastPage(),
            'previous' => $data->getCurrentPage() - 1 <= 0 ? 1 : $data->getCurrentPage() - 1,
            'next' => $data->getCurrentPage() + 1 > $data->getLastPage() ? $data->getLastPage() : $data->getCurrentPage() + 1,
            'totalItems' => $data->getTotalItems(),
            'itemsPerPage' => $data->getItemsPerPage(),
            'currentPerPage' => count($data)
        ];
    }

    public function getMetadataPaginationSpecific($data , $totalItems = 0){
        $count = count($data);
        $nbPageReturned = 0;
        if($count > 0){
            $nbPageReturned = $data->getTotalItems() / /*count($data)*/$data->getItemsPerPage();
        }
        $nbPageReturned = ceil($nbPageReturned);
        $links = [];
        for($i=1 ; $i<= $nbPageReturned;$i++){
            $links[] = [
                "url" => "?/page=$i",
                "active" => $i == $data->getCurrentPage(),
                "page" => $i
            ];
        }
        return [
            "links" => $links,
            'first' => 1,
            'current' => $data->getCurrentPage(),
            'last' => $data->getLastPage(),
            'previous' => $data->getCurrentPage() - 1 <= 0 ? 1 : $data->getCurrentPage() - 1,
            'next' => $data->getCurrentPage() + 1 > $data->getLastPage() ? $data->getLastPage() : $data->getCurrentPage() + 1,
            'totalItems' => $data->getTotalItems(),
            'itemsPerPage' => $data->getItemsPerPage(),
            'currentPerPage' => count($data)
        ];
    }
}