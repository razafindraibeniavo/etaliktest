<?php
namespace App\EventSubscriber;

use ApiPlatform\Symfony\EventListener\EventPriorities;
use App\Service\ErrorCodeService;
use App\Service\PaginationService;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Doctrine\Common\EventSubscriber;
use UnexpectedValueException;
use Symfony\Contracts\Translation\TranslatorInterface;

class CustomSerializationSubscriber implements EventSubscriberInterface
{
    private $responseStatus;

    public function __construct(
        private PaginationService $paginationService,
        private EntityManagerInterface $em,
        private ErrorCodeService $errorCodeService,
        private TranslatorInterface $translator
    )
    {
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::RESPONSE => ['onKernelResponse', EventPriorities::POST_RESPOND],
            KernelEvents::VIEW => ['addExtraData', EventPriorities::POST_SERIALIZE],
            KernelEvents::EXCEPTION => ['onKernelException', EventPriorities::POST_SERIALIZE],
        ];
    }

    public function onKernelResponse(ResponseEvent $event): void
    {
//        dd($event->getRequest()->attributes->get("_route"));
        if((!in_array($event->getRequest()->attributes->get("_route") , [
//                "gesdinet_jwt_refresh_token",
//                "api_login_check",
//                "app_logout",
                "api_entrypoint",
                "_api_/api/logout_get",
                "_api_/getin/{id}/print_get",
                "_api_/getout/{id}/print_get",
                "_api_/proforma/{id}/print_get",
                "app_list_document",
                "app_document_index",
                "app_document_new",
                "_api_/documents/{id}{._format}_get",
                "app_document_show",
                "app_document_edit",
                "app_upload_document",
                "app_document_new"
            ])
            )
            ||
            (in_array($event->getRequest()->attributes->get("_route") , [
                "gesdinet_jwt_refresh_token",
                "api_login_check",
            ])
            && !$event->getResponse()->isOk()
            )
        ) {
            $apiOperation = $event->getRequest()->attributes->get('_api_operation');
            $content = ($event->getResponse()->getContent());
//            dd($apiOperation->getOpenapiContext()["summary"]);
            $contentArray = (json_decode($content, true));
            $contentArray["status"] = [];
            $contentArray["status"]["code"] = $event->getResponse()->getStatusCode();
            $contentArray["status"]["success"] = $event->getResponse()->getStatusCode() >= 200 && $event->getResponse()->getStatusCode() <= 299;
            $contentArray["status"]["message"] = $contentArray["message"] ?? $apiOperation?->getOpenapiContext()["summary"] ?? "";
            $contentArray["status"]["messageDetail"] = $contentArray["messageDetail"] ?? "noMessage";
            $contentArray["status"]["errorCode"] = !$event->getResponse()->isOk()  ? $this->errorCodeService->getStatusFormatCode($event->getResponse()->getStatusCode()) : "noError";
            $contentArray["status"]["detail"] = $contentArray["detail"] ?? "noDetail";
            unset($contentArray["code"]);
            unset($contentArray["message"]);
            unset($contentArray["messageDetail"]);
            unset($contentArray["detail"]);
            if(!isset($contentArray["token"])){
                unset($contentArray["refresh_token"]);
            }
            $event->getResponse()->setContent(json_encode($contentArray));
        }
    }

    public function addExtraData(ViewEvent $event)
    {
//        dd($event->getRequest()->attributes->get("_route"));
        // Récupérez l'objet à sérialiser
        $data = $event->getRequest()->attributes->get('data');
        // Perform the pagination result
        $object = $event->getControllerResult();
        $newData = json_decode($object, true);
        if(str_contains($event->getRequest()->attributes->get("_route") , "_get_collection")
            &&
            !in_array($event->getRequest()->attributes->get("_route") , [
                "_api_/operator/{operatorId}/receiptSheetArticle_get_collection",
                "_api_/profile/users_get_collection",
                "_api_/exportAllGetins_get_collection",
                "_api_/exportAllGetouts_get_collection",
                "_api_/regulation_types{._format}_get_collection",
                "_api_/documents{._format}_get_collection"
            ])
        )
        {
            $status = null;
            $formatData["status"] = $this->responseStatus;
            $formatData["data"] = $newData;
            $formatData["payload"]["pagination"] = $this->paginationService->getMetadataPagination($data);
            $event->setControllerResult(json_encode($formatData,true));
        }
        else{
            $status = null;
            $formatData["status"] = $this->responseStatus;
            $formatData["data"] = $newData;
            $formatData["payload"]["pagination"] = null;
            $event->setControllerResult(json_encode($formatData,true));
        }
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        $event->setResponse(new JsonResponse([
            "detail"  => "Fichier ".$exception->getFile().' sur la ligne '.$exception->getLine(),
            "message" => $exception->getLine() ? "Une erreur s'est produite" : $exception->getMessage(),
            "messageDetail" => $exception->getMessage(),
        ]));
    }
}