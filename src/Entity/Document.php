<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Repository\DocumentRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: DocumentRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            openapiContext: [
                "summary" => "Liste des document",
            ],
            normalizationContext: ['groups' => 'document:read'],
        ),
        new Get(
            openapiContext: [
                "summary" => "Détail d'un document",
            ],
            normalizationContext: ['groups' => 'document:read'],
        ),
        new Post(
            openapiContext: [
                "summary" => "Ajout d'un document",
            ],
            normalizationContext: ['groups' => 'document:read'],
            denormalizationContext: ['groups' => 'document:create'],
        ),
        new Patch(
            openapiContext: [
                "summary" => "Modification d'un document",
            ],
            normalizationContext: ['groups' => 'document:read'],
            denormalizationContext: ['groups' => 'document:update'],
        ),
        new Delete(
            openapiContext: [
                "summary" => "Suppression d'un document",
            ],
            security: "is_granted('PERMISSION_ALLOWED' , object)",
        ),
    ],
    paginationEnabled: false
)]
#[ApiFilter(
    OrderFilter::class,properties: [
    "id",
],arguments:  ['orderParameterName' => 'order'])
]
#[UniqueEntity(
    [
        'compteAffaire','compteEvent','compteDerEv','numFiche','libelleCiv','proprioVehicule','nom','prenom','numVoie','compAdresse','codePostal','ville','telDom',
        'telPor','telJob','email','dateCirculation','dateAchat','dateDerEnv','libelleMarque','libelleModele','version','vin','immatriculation','typeProspect',
        'kilometrage','energie','vendeurVn','vendeurVO','commentaireFacture','typeVnVo','numDVnVo','numDVnVo','DateEv','originEnv'
    ], message: 'duplication des données ', ignoreNull: false)]
class Document
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(
        identifier: true,
    )]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $compteAffaire = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $compteEvent = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $compteDerEv = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $numFiche = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $libelleCiv = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $proprioVehicule = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $nom = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $prenom = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $numVoie = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $compAdresse = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $codePostal = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $ville = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $telDom = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $telPor = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $telJob = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $email = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?\DateTimeInterface $dateCirculation = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?\DateTimeInterface $dateAchat = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?\DateTimeInterface $dateDerEnv = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $libelleMarque = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $libelleModele = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $version = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $vin = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $immatriculation = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $typeProspect = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $kilometrage = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $energie = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $vendeurVn = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $vendeurVO = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $commentaireFacture = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $typeVnVo = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $numDVnVo = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $interVen = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?\DateTimeInterface $DateEv = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(groups: [
        'document:create','document:update','document:read',
    ])]
    private ?string $originEnv = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompteAffaire(): ?string
    {
        return $this->compteAffaire;
    }

    public function setCompteAffaire(?string $compteAffaire): self
    {
        $this->compteAffaire = $compteAffaire;

        return $this;
    }

    public function getCompteEvent(): ?string
    {
        return $this->compteEvent;
    }

    public function setCompteEvent(?string $compteEvent): self
    {
        $this->compteEvent = $compteEvent;

        return $this;
    }

    public function getCompteDerEv(): ?string
    {
        return $this->compteDerEv;
    }

    public function setCompteDerEv(?string $compteDerEv): self
    {
        $this->compteDerEv = $compteDerEv;

        return $this;
    }

    public function getNumFiche(): ?string
    {
        return $this->numFiche;
    }

    public function setNumFiche(?string $numFiche): self
    {
        $this->numFiche = $numFiche;

        return $this;
    }

    public function getLibelleCiv(): ?string
    {
        return $this->libelleCiv;
    }

    public function setLibelleCiv(?string $libelleCiv): self
    {
        $this->libelleCiv = $libelleCiv;

        return $this;
    }

    public function getProprioVehicule(): ?string
    {
        return $this->proprioVehicule;
    }

    public function setProprioVehicule(?string $proprioVehicule): self
    {
        $this->proprioVehicule = $proprioVehicule;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNumVoie(): ?string
    {
        return $this->numVoie;
    }

    public function setNumVoie(?string $numVoie): self
    {
        $this->numVoie = $numVoie;

        return $this;
    }

    public function getCompAdresse(): ?string
    {
        return $this->compAdresse;
    }

    public function setCompAdresse(?string $compAdresse): self
    {
        $this->compAdresse = $compAdresse;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(?string $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getTelDom(): ?string
    {
        return $this->telDom;
    }

    public function setTelDom(?string $telDom): self
    {
        $this->telDom = $telDom;

        return $this;
    }

    public function getTelPor(): ?string
    {
        return $this->telPor;
    }

    public function setTelPor(?string $telPor): self
    {
        $this->telPor = $telPor;

        return $this;
    }

    public function getTelJob(): ?string
    {
        return $this->telJob;
    }

    public function setTelJob(?string $telJob): self
    {
        $this->telJob = $telJob;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getDateCirculation(): ?\DateTimeInterface
    {
        return $this->dateCirculation;
    }

    public function setDateCirculation(?\DateTimeInterface $dateCirculation): self
    {
        $this->dateCirculation = $dateCirculation;

        return $this;
    }

    public function getDateAchat(): ?\DateTimeInterface
    {
        return $this->dateAchat;
    }

    public function setDateAchat(?\DateTimeInterface $dateAchat): self
    {
        $this->dateAchat = $dateAchat;

        return $this;
    }

    public function getDateDerEnv(): ?\DateTimeInterface
    {
        return $this->dateDerEnv;
    }

    public function setDateDerEnv(?\DateTimeInterface $dateDerEnv): self
    {
        $this->dateDerEnv = $dateDerEnv;

        return $this;
    }

    public function getLibelleMarque(): ?string
    {
        return $this->libelleMarque;
    }

    public function setLibelleMarque(?string $libelleMarque): self
    {
        $this->libelleMarque = $libelleMarque;

        return $this;
    }

    public function getLibelleModele(): ?string
    {
        return $this->libelleModele;
    }

    public function setLibelleModele(?string $libelleModele): self
    {
        $this->libelleModele = $libelleModele;

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(?string $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getVin(): ?string
    {
        return $this->vin;
    }

    public function setVin(?string $vin): self
    {
        $this->vin = $vin;

        return $this;
    }

    public function getImmatriculation(): ?string
    {
        return $this->immatriculation;
    }

    public function setImmatriculation(?string $immatriculation): self
    {
        $this->immatriculation = $immatriculation;

        return $this;
    }

    public function getTypeProspect(): ?string
    {
        return $this->typeProspect;
    }

    public function setTypeProspect(?string $typeProspect): self
    {
        $this->typeProspect = $typeProspect;

        return $this;
    }

    public function getKilometrage(): ?string
    {
        return $this->kilometrage;
    }

    public function setKilometrage(?string $kilometrage): self
    {
        $this->kilometrage = $kilometrage;

        return $this;
    }

    public function getEnergie(): ?string
    {
        return $this->energie;
    }

    public function setEnergie(?string $energie): self
    {
        $this->energie = $energie;

        return $this;
    }

    public function getVendeurVn(): ?string
    {
        return $this->vendeurVn;
    }

    public function setVendeurVn(?string $vendeurVn): self
    {
        $this->vendeurVn = $vendeurVn;

        return $this;
    }

    public function getVendeurVO(): ?string
    {
        return $this->vendeurVO;
    }

    public function setVendeurVO(?string $vendeurVO): self
    {
        $this->vendeurVO = $vendeurVO;

        return $this;
    }

    public function getCommentaireFacture(): ?string
    {
        return $this->commentaireFacture;
    }

    public function setCommentaireFacture(?string $commentaireFacture): self
    {
        $this->commentaireFacture = $commentaireFacture;

        return $this;
    }

    public function getTypeVnVo(): ?string
    {
        return $this->typeVnVo;
    }

    public function setTypeVnVo(?string $typeVnVo): self
    {
        $this->typeVnVo = $typeVnVo;

        return $this;
    }

    public function getNumDVnVo(): ?string
    {
        return $this->numDVnVo;
    }

    public function setNumDVnVo(?string $numDVnVo): self
    {
        $this->numDVnVo = $numDVnVo;

        return $this;
    }

    public function getInterVen(): ?string
    {
        return $this->interVen;
    }

    public function setInterVen(?string $interVen): self
    {
        $this->interVen = $interVen;

        return $this;
    }

    public function getDateEv(): ?\DateTimeInterface
    {
        return $this->DateEv;
    }

    public function setDateEv(?\DateTimeInterface $DateEv): self
    {
        $this->DateEv = $DateEv;

        return $this;
    }

    public function getOriginEnv(): ?string
    {
        return $this->originEnv;
    }

    public function setOriginEnv(?string $originEnv): self
    {
        $this->originEnv = $originEnv;

        return $this;
    }
}
