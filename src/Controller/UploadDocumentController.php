<?php

namespace App\Controller;

use App\Entity\Document;
use App\Form\FileUploadType;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UploadDocumentController extends AbstractController
{
    /**
     * @throws \Exception
     */
    #[Route('/upload', name: 'app_upload_document')]
    public function index(Request $request , FileUploader $file_uploader, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(FileUploadType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $file = $form['upload_file']->getData();
            if ($file)
            {
                $file_name = $file_uploader->upload($file);
                if (null !== $file_name) // for example
                {
                    $directory = $file_uploader->getTargetDirectory();
                    $full_path = $directory.'/'.$file_name;
                    // Do what you want with the full path file...
                    // Why not read the content or parse it !!!
                    // Open the file
                    $spreadsheet = IOFactory::load($full_path);
                    $row = $spreadsheet->getActiveSheet()->removeRow(1); // I added this to be able to remove the first file line
                    $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
                    foreach($sheetData as $data){
                        $document = new Document();
                        $document->setCompteAffaire($data["A"]);$document->setCompteEvent($data["B"]);$document->setCompteDerEv($data["C"]);
                        $document->setNumFiche($data["D"]);$document->setLibelleCiv($data["E"]);$document->setProprioVehicule($data["F"]);
                        $document->setNom($data["G"]);$document->setPrenom($data["H"]);$document->setNumVoie($data["I"]);
                        $document->setCompAdresse($data["J"]);$document->setCodePostal($data["K"]);$document->setVille($data["L"]);
                        $document->setTelDom($data["M"]);$document->setTelPor($data["N"]);$document->setTelJob($data["O"]);
                        $document->setEmail($data["P"]);$document->setDateCirculation(new \DateTime(date("Y-m-d",strtotime($data["Q"]))));$document->setDateAchat(new \DateTime(date("Y-m-d",strtotime($data["R"]))));
                        $document->setDateDerEnv(new \DateTime(date("Y-m-d",strtotime($data["S"]))));$document->setLibelleMarque($data["T"]);$document->setLibelleModele($data["U"]);
                        $document->setVersion($data["V"]);$document->setVin($data["W"]);$document->setImmatriculation($data["X"]);
                        $document->setTypeProspect($data["Y"]);$document->setKilometrage($data["Z"]);$document->setEnergie($data["AA"]);
                        $document->setVendeurVn($data["AB"]);$document->setVendeurVO($data["AC"]);$document->setCommentaireFacture($data["AD"]);
                        $document->setTypeVnVo($data["AE"]);$document->setNumDVnVo($data["AF"]);$document->setInterVen($data["AG"]);
                        $document->setDateEv(new \DateTime(date("Y-m-d",strtotime($data["AH"]))));$document->setOriginEnv($data["AI"]);
                        $em->persist($document);
                        $em->flush();
                    }
                    $this->addFlash('success', 'Opération réussie!');
                    unlink($full_path);
                }
                else
                {
                    // Oups, an error occured !!!
                }
            }
        }

        return $this->render('upload_document/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    #[Route('/', name: 'app_list_document')]
    public function list(Request $request , EntityManagerInterface $em): Response
    {
        return $this->render('document/list-document.html.twig');
    }
}
