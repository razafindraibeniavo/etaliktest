$(document).ready(function () {
    var table = $("#example").DataTable({
        ajax: {
            url: '/api/documents?order[id]=asc',
        },
        columns: [
            { data: "compteAffaire" },
            { data: "compteEvent" },
            { data: "compteDerEv" },
            { data: "numFiche" },
            { data: "libelleCiv" },
            { data: "proprioVehicule" },
            { data: "nom" },
            { data: "prenom" },
            { data: "numVoie" },
            { data: "compAdresse" },
            { data: "codePostal" },
            { data: "ville" },
            { data: "telDom" },
            { data: "telPor" },
            { data: "telJob" },
            { data: "email" },
            { data: "dateCirculation" },
            { data: "dateAchat" },
            { data: "dateDerEnv" },
            { data: "libelleMarque" },
            { data: "libelleModele" },
            { data: "version" },
            { data: "vin" },
            { data: "immatriculation" },
            { data: "typeProspect" },
            { data: "kilometrage" },
            { data: "energie" },
            { data: "vendeurVn" },
            { data: "vendeurVO" },
            { data: "commentaireFacture" },
            { data: "typeVnVo" },
            { data: "numDVnVo" },
            { data: "interVen" },
            { data: "DateEv" },
            { data: "originEnv" },
            {
                data: "action" ,
                render: function (data, type, row) {
                    console.log(row);
                    return `
                        <a href="/document/${row.id}" class="btn btn-primary">Voir</a>
                        <a href="/document/${row.id}/edit" class="btn btn-success">Update</a>
                    `;
                }
            },
        ],
    });
});